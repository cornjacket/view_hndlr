module bitbucket.org/cornjacket/view_hndlr

go 1.13

require (
	bitbucket.org/cornjacket/iot v0.1.7
	github.com/go-openapi/runtime v0.19.21
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/context v1.1.1
	github.com/julienschmidt/httprouter v1.3.0
	github.com/segmentio/kafka-go v0.4.2
	gopkg.in/check.v1 v1.0.0-20200902074654-038fdea0a05b
)

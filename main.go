package main

import (
	"bitbucket.org/cornjacket/view_hndlr/app"
)

func main() {

	viewApp := app.NewAppService()	
	viewApp.Init()
	viewApp.Run()

}

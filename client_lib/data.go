package client_lib

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

// TODO: There is code duplication with the Data struct. It lives within the app as well as this client lib. It should only live in one place,
//       probably in the client lib only.
// Note that Data type is copied from view_hndlr_4_app3_sys6/dataproc. By copying it instead of importing it as a dependency the event_hndlr
// can be built independent of the view_hndlr. It is imperative that any changes to Data in the view_hndlr uService be refected in this service,
// and vice versa.
type Data struct {
	Eui     string // this should be intrinsic_id, we can have LoraEui and Eui (intrinsic)
	Ts      uint64
	ValueA  int
	ValueB  int
	CorrId  uint64 `json:"corrid"`
	ClassId string `json:"classid"`
	Hops    int    `json:"hops"`
}

func (s *ViewHndlrService) SendStatus(data Data) error {

	var err error
	if s.Enabled() {
		//fmt.Printf("ViewHndlrService: Sending data to %s\n", s.URL("/data"))
		b := new(bytes.Buffer)
		json.NewEncoder(b).Encode(data)
		err = Post(s.URL("/data"), b)
	} else {
		fmt.Printf("ViewHndlrService is disabled. Unable to send status data.\n")
	}
	return err

}

// DataRecord is a subset of data.Row
type DataRecord struct {
	Ts     int64 `json:"ts"`
	ValueA int   `json:"valuea"`
	ValueB int   `json:"valueb"`
}

type DataStateResp struct {
	TsStart uint64       `json:"start"`
	TsEnd   uint64       `json:"end"`
	Eui     string       `json:"eui"`
	Data    []DataRecord `json:"data"`
}

// TODO: The following types should be extracted into a separate module...
type DataStateReq struct {
	TsStart uint64 `json:"start"`
	TsEnd   uint64 `json:"end"`
	Eui     string `json:"eui"`
}

func (d *DataStateReq) Validate() error {
	if d.Eui == "" {
		return errors.New("Required Eui")
	}
	return nil
}

func (s *ViewHndlrService) GetData(req DataStateReq) (DataStateResp, error) {

	fmt.Printf("ViewHndlrService.GetData Eui: %s, start: %d, end: %d\n", req.Eui, req.TsStart, req.TsEnd)
	var resp DataStateResp
	if !s.Enabled() {
		return resp, errors.New("Service not enabled")
	}
	reqBody, err := json.Marshal(req)
	if err != nil {
		return resp, err
	}
	//url := s.URL("/data")
	url := s.URL(s.Config.DataPath)
	client := &http.Client{}
	httpReq, err := http.NewRequest("GET", url, bytes.NewBuffer(reqBody))
	httpReq.Header.Set("Content-type", "application/json")
	if err != nil {
		return resp, err
	}
	response, err := client.Do(httpReq)

	if err != nil {
		return resp, err
	} else {
		defer response.Body.Close()
		contents, err := ioutil.ReadAll(response.Body)
		if err != nil {
			return resp, err
		}
		err = json.Unmarshal(contents, &resp)
		if err != nil {
			return resp, err
		}
	}
	return resp, err
}

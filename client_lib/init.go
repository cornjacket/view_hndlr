package client_lib

import (
	"errors"
	"fmt"

	kafka "github.com/segmentio/kafka-go"
)

type TransportType int

const (
	Disabled TransportType = 0 // default
	HttpPost TransportType = 1
	Kafka    TransportType = 2
)

// TODO(DRT) - For now the Kafka functionality only works with the PacketTopic. The Nodestate and Data path will still
// use HttpPost until I decide it is time to move to Kafka.

// if KafkaUrl is given, then client behaves in Loosely-coupled mode using Kafka.
// Otherwise client behaves in tightly-coupled mode using http Post.
type ViewHndlrServiceConfig struct {
	TransportType  TransportType
	KafkaHostname  string
	KafkaPort      string
	PacketTopic    string // used for topic for kafka
	NodestateTopic string // used for topic for kafka
	DataTopic      string // use for topic for kafka
	Hostname       string // specifies http_post endpoint
	Port           string // specifies http_post port
	PacketPath     string // used as path for http_post
	NodestatePath  string // used as path for http post
	DataPath       string // used as path for http post
}

// TODO(DRT) - Do I still need Enabled as a field inside Service struct
/* OLD
type ViewHndlrService struct {
	Hostname string
	Port     string
	Enabled  bool
}
*/

type ViewHndlrService struct {
	Config      ViewHndlrServiceConfig
	KafkaWriter *kafka.Writer
}

// TODO: Validate Hostname and Port within reason
func (s *ViewHndlrService) Open(c ViewHndlrServiceConfig) error {
	s.Config = c
	if s.Config.TransportType == HttpPost {
		if c.Hostname == "" || c.Port == "" || c.PacketPath == "" || c.NodestatePath == "" || c.DataPath == "" {
			return errors.New("Invalid Http configuration")
		}
		return s.Ping()
	} else { // kafka

		if c.KafkaHostname == "" || c.KafkaPort == "" || c.PacketTopic == "" || c.NodestateTopic == "" || c.DataTopic == "" {
			return errors.New("Invalid Kakfa configuration")
		}
		// get kafka writer using environment variables.
		// TODO(DRT) The following needs to change and use the config passed in as param.
		kafkaURL := s.Config.KafkaHostname + ":" + s.Config.KafkaPort //os.Getenv("kafkaURL")
		topic := s.Config.PacketTopic                                 //os.Getenv("topic")
		fmt.Printf("eventHndlrService: kafka open %s, %s\n", kafkaURL, topic)
		s.KafkaWriter = getKafkaWriter(kafkaURL, topic)

		// TODO(DRT) where does the kafka writer get closed? Is there a deferral that can be done somewhere
		// Maybe there needs to be an app.Close function that gets deferred that closes kafka as well as the DB
		//defer kafkaWriter.Close()

		// TODO(DRT) validate that kafkaHostname, port, and topic are not empty and if any are
		// then responsd with custom error
		return nil // for now we won't doing anything. Later we want to know if kafka broker/topic is up/created
	}
}

func getKafkaWriter(kafkaURL, topic string) *kafka.Writer {
	return kafka.NewWriter(kafka.WriterConfig{
		Brokers:  []string{kafkaURL},
		Topic:    topic,
		Balancer: &kafka.LeastBytes{},
	})
}

func (s *ViewHndlrService) Enabled() bool {
	return s.Config.TransportType != Disabled
}

// TODO: This needs to be done...
// Used to determine if the dependent service is currently up.
// Ping should make multiple attempts to see if the external service is up before giving up and returning an error.
func (s *ViewHndlrService) Ping() error {
	// check if enabled
	// if so, then make a GET call to /ping of the URL()
	// if there is an error, then repeatedly call for X times with linear backoff until success
	/* Keep and add to services...
	   // ping loop
	   var pingErr error
	   for i := 0; i < 15; i++ {
	           fmt.Printf("Pinging %s\n", DbHost)
	           pingErr = server.DB.DB().Ping()
	           if pingErr != nil {
	                   fmt.Println(pingErr)
	           } else {
	                   fmt.Println("Ping replied.")
	                   break;
	           }
	           time.Sleep(time.Duration(3) * time.Second)
	   }
	   if pingErr != nil {
	           log.Fatal("This is the error:", err)
	   }
	*/

	return nil
}

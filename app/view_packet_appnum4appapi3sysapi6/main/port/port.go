package port

import (
  "strconv"
)

type Port struct {
  value int
  updated bool
}

func New() *Port {
  return &Port{}
}

func (x *Port) IsUpdated() bool {
  return x.updated
}

func (x *Port) Get() int {
  return x.value
}

func (x *Port) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Port) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *Port) String() string {
  return "Port"
}

func (x *Port) Type() string {
  return "int"
}

func (x *Port) Modifiers() string {
  return "not null"
}

func (x *Port) QueryValue() string {
  return strconv.Itoa(x.value)
}


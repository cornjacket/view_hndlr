package cmd

import (
)

type Cmd struct {
  value string
  updated bool
}

func New() *Cmd {
  return &Cmd{}
}

func (x *Cmd) IsUpdated() bool {
  return x.updated
}

func (x *Cmd) Get() string {
  return x.value
}

func (x *Cmd) Set(value string) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Cmd) Load(value string) {
  x.value = value
  x.updated = false
}

func (x *Cmd) String() string {
  return "Cmd"
}

func (x *Cmd) Type() string {
  return "varchar(22)"
}

func (x *Cmd) Modifiers() string {
  return "not null"
}

func (x *Cmd) QueryValue() string {
  if x.value == "" {
    return "0"
  }
  return "\"" + x.value + "\""
}


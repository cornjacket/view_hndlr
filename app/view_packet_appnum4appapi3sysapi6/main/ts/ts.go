package ts

import (
  "strconv"
)

type Ts struct {
  value int64
  updated bool
}

func New() *Ts {
  return &Ts{}
}

func (x *Ts) IsUpdated() bool {
  return x.updated
}

func (x *Ts) Get() int64 {
  return x.value
}

func (x *Ts) Set(value int64) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *Ts) Load(value int64) {
  x.value = value
  x.updated = false
}

func (x *Ts) String() string {
  return "Ts"
}

func (x *Ts) Type() string {
  return "bigint"
}

func (x *Ts) Modifiers() string {
  return "not null"
}

func (x *Ts) QueryValue() string {
  return strconv.FormatInt(x.value, 10)
}


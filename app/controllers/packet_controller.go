package controllers

import (
	// Standard library packages
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"bitbucket.org/cornjacket/iot/message"
	"bitbucket.org/cornjacket/iot/serverlib/pktproc2"
	packet "bitbucket.org/cornjacket/view_hndlr/app/view_packet_appnum4appapi3sysapi6"
	"github.com/julienschmidt/httprouter"
)

// A message.UpPacket is returned in the response
// swagger:response packetResponse
type packetResponseWrapper struct {
	// Packet send to Event Handler
	// in: body
	Body message.UpPacket
}

// swagger:route POST /packet packet sendPacket
// Sends a LoRa Rx packet to the View Handler for storage.
// Returns LoRa packet to client
// responses:
//   201: packetResponse

// This function returns the entry point for the packet processing worker pool upon reception of
// a post to /packet. The default behavior is for the pktproc2 post worker to immediately queue the packet
// followed by replying to the client with the posted packet.
func (app *AppContext) NewParserWorkerPoolFunc(numWorkers int) func(http.ResponseWriter, *http.Request, httprouter.Params) {
	return pktproc2.NewHttpPacketHandlerFunc(numWorkers, app.packetHandler)
}

func (app *AppContext) packetHandler(wID int, p message.UpPacket) bool {

	// Ingest packet into packet table
	err := insertPktTable(&p)
	pktproc2.DisplayPassFailMessageWithLatency("Ingest to packet table", err, wID, &p, true)
	return true // this should be returning the error

}

func insertPktTable(p *message.UpPacket) error {

	pkt := packet.New()
	pktRow := packet2pktRow(p)
	err := pkt.Insert(&pktRow)
	fmt.Printf("*******************************Insert Packet\n")
	return err

}

// This is a standard function converting from loriot packet to packet SQL row datatype. For now this func lives in the packet controller but
// in the future the loriot packet table could become a standard structure allowing this function to become common.
func packet2pktRow(p *message.UpPacket) packet.Row {

	pktRow := packet.Row{}
	pktRow.Ts = int64(p.Ts)
	pktRow.Dr = p.Dr
	pktRow.Eui = p.Eui
	if p.Ack {
		pktRow.Ack = 1
	}
	pktRow.Cmd = p.Cmd
	pktRow.Snr = int(p.Snr * 10)
	pktRow.Data = p.Data
	pktRow.Fcnt = p.Fcnt
	pktRow.Freq = int64(p.Freq) // TODO: Is p.Freq too small to hold the freq?
	pktRow.Port = p.Port
	pktRow.Rssi = p.Rssi
	return pktRow

}

// web backend will request packet to be retrieved and returned by this controller
type PacketReq struct {
	TsStart uint64 `json:"start"`
	TsEnd   uint64 `json:"end"`
	Eui     string `json:"eui"`
}

// UpPacket is a subset of message.UpPacket
type UpPacket struct {
	Dr   string  `json:"dr"`
	Ts   uint64  `json:"ts"`
	Ack  bool    `json:"ack"`
	Snr  float32 `json:"snr"`
	Data string  `json:"data"`
	Fcnt int     `json:"fcnt"`
	Freq uint64  `json:"freq"`
	Port int     `json:"port"`
	Rssi int     `json:"rssi"`
}

type PacketsResp struct {
	TsStart uint64     `json:"start"`
	TsEnd   uint64     `json:"end"`
	Eui     string     `json:"eui"`
	Packets []UpPacket `json:"packets"`
}

// A PacketsResp is returned in the response
// swagger:response packetGetResponse
type packetGetResponseWrapper struct {
	// packet received from View Handler
	// in: body
	Body PacketsResp
}

// swagger:route GET /packet packet listPackets
// Retrieves packet from the View Handler's database storage.
// Returns PacketsResp to client
// responses:
//   200: packetGetResponse
// reads data from db and returns slice of Rows to caller
func (app *AppContext) GetPacketHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var statusCode = 200

	// Stub a data req to be populated from the body
	req := PacketReq{}

	// Populate the data
	json.NewDecoder(r.Body).Decode(&req)

	resp := createPacketsResp(&req)

	if validPacketRequest(&req) {
		p := packet.New()
		err, packetList := p.SelectAllFromCurrent(req.Eui)

		displayPassFailPacketMessage("Select All From Packet Store", err, &req)
		if err != nil {
			statusCode = 400 // TODO(drt): Change to failure constant
		} else {
			// copy over each data element to data array
			for _, entry := range packetList {
				resp.Packets = append(resp.Packets, newUpPacket(&entry))
				fmt.Printf("********************************** DEBUG 2: %v\n", resp.Packets)
				fmt.Printf("********************************** DEBUG 2: length = %d\n", len(resp.Packets))
			}
		}
	} else {
		displayFailPacketMessage("DataReq is not valid", &req)
		statusCode = 200 // TODO(drt): Change to failure constant
	}

	// Marshal provided interface into JSON strucutre
	respj, _ := json.Marshal(resp)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	fmt.Fprintf(w, "%s", respj)
}

// TODO(drt) need to check if Eui is not emptry. Maybe add some other checks.
func validPacketRequest(req *PacketReq) bool {
	if req.Eui == "" {
		return false
	}
	return true
}

func createPacketsResp(req *PacketReq) PacketsResp {
	resp := PacketsResp{}
	resp.TsStart = req.TsStart
	resp.TsEnd = req.TsEnd
	resp.Eui = req.Eui
	resp.Packets = nil
	return resp
}

func displayPassFailPacketMessage(message string, err error, req *PacketReq) {

	if err == nil {
		fmt.Printf("PASS, EUI: %s, tsStart: %d, tsEnd: %d, %s\n", req.Eui, req.TsStart, req.TsEnd, message)
	} else {
		fmt.Printf("FAIL, EUI: %s, tsStart: %d, tsEnd: %d, %s, err: %s\n", req.Eui, req.TsStart, req.TsEnd, message, err)
	}
}

func displayFailPacketMessage(message string, req *PacketReq) {

	fmt.Printf("FAIL, EUI: %s, tsStart: %d, tsEnd: %d, %s\n", req.Eui, req.TsStart, req.TsEnd, message)
}

func newUpPacket(p *packet.Row) UpPacket {
	upPacket := UpPacket{}
	upPacket.Dr = p.Dr
	upPacket.Ts = uint64(p.Ts)
	if p.Ack == 0 {
		upPacket.Ack = false
	} else {
		upPacket.Ack = true
	}
	upPacket.Snr = float32(p.Snr) / 10.0
	upPacket.Data = p.Data
	upPacket.Fcnt = p.Fcnt
	upPacket.Freq = uint64(p.Freq)
	upPacket.Port = p.Port
	upPacket.Rssi = p.Rssi
	return upPacket
}

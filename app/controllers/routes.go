package controllers

import (
	"net/http"

	"github.com/go-openapi/runtime/middleware"
	"github.com/gorilla/context"
	"github.com/julienschmidt/httprouter"
)

func (app *AppContext) initializeRoutes(kafkaEnabled bool) {

	// Packet Route
	// pkt proc only supports either kafka or http workers at one time, but not both
	if !kafkaEnabled {
		// Packet Route
		app.Router.POST("/packet", app.NewParserWorkerPoolFunc(100))
	}
	app.Router.GET("/packet", app.GetPacketHandler)

	// Data Ingestion Route
	app.Router.POST("/data", app.NewDataWorkerPoolFunc(100))
	// Data Query Route
	app.Router.GET("/data", app.GetStateDataHandler) // <--- This is wrong. It should be get /data not get /state

	// Home Route
	app.Router.GET("/", wrapHandlerFunc(app.Home))

	// Shadow Event_Hndlr Route
	app.Router.POST("/nodestate", app.ShadowNodeState)
	app.Router.GET("/nodestate/:eui", app.GetNodeState)

	// Swagger API /docs path
	opts := middleware.RedocOpts{SpecURL: "/swagger.yaml"}
	sh := middleware.Redoc(opts, nil)
	app.Router.GET("/docs", wrapHandlerFunc(sh.ServeHTTP))
	app.Router.GET("/swagger.yaml", wrapHandlerFunc(http.FileServer(http.Dir("./app")).ServeHTTP))

	// TODO:
	// Add /ping
	// Add /health
	// Add version, lastboot, ... via status. See code in temp

}

// Wrapper function to make http handlers (i.e. goriall/mux) work with httprouter (i.e. julienschmidt)
// gorilla/context may be useful in the future if other http handlers (besides home) need to be converted to httprouter.
// source: https://www.nicolasmerouze.com/guide-routers-golang
func wrapHandlerFunc(h http.HandlerFunc) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		context.Set(r, "params", ps)
		h.ServeHTTP(w, r)
	}
}

package controllers

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	data "bitbucket.org/cornjacket/view_hndlr/app/data_appnum4_appapi3_sysapi6"
	"bitbucket.org/cornjacket/view_hndlr/app/dataproc"
	"github.com/julienschmidt/httprouter"
)

// A data.Row is returned in the response
// swagger:response dataPostResponse
type dataPostResponseWrapper struct {
	// data sent to View Handler
	// in: body
	Body data.Row
}

// swagger:route POST /data data sendData
// Sends data to the View Handler for database storage.
// Returns data Row to client
// responses:
//   201: dataPostResponse
// This function returns the entry point for the post to /data
func (app *AppContext) NewDataWorkerPoolFunc(numWorkers int) func(http.ResponseWriter, *http.Request, httprouter.Params) {
	return dataproc.NewDataHandlerFunc(numWorkers, app.dataIngestionHandler)
}

func (app *AppContext) dataIngestionHandler(wID int, d dataproc.Data) bool {

	dataproc.DisplayStatus(wID, &d)
	dataRow := data.Row{}
	dataRow.Eui = d.Eui
	dataRow.Ts = int64(d.Ts)
	dataRow.ValueA = d.ValueA
	dataRow.ValueB = d.ValueB

	data := data.New()
	err := data.Insert(&dataRow)
	dataproc.DisplayPassFailMessageWithLatency("Insert into Data table", err, wID, &d, true)

	return true
}

// web backend will request data to be retrieved and returned by this controller
// later this will/could have 2 separate handlers. one for data state A and one or data state B
type DataReq struct {
	TsStart uint64 `json:"start"`
	TsEnd   uint64 `json:"end"`
	Eui     string `json:"eui"`
}

// DataRecord is a subset of data.Row
type DataRecord struct {
	Ts     int64 `json:"ts"`
	ValueA int   `json:"valuea"`
	ValueB int   `json:"valueb"`
}

type DataResp struct {
	TsStart uint64       `json:"start"`
	TsEnd   uint64       `json:"end"`
	Eui     string       `json:"eui"`
	Data    []DataRecord `json:"data"`
}

// A DataResp is returned in the response
// swagger:response dataGetResponse
type dataGetResponseWrapper struct {
	// data received from View Handler
	// in: body
	Body DataResp
}

// swagger:route GET /data data listData
// Retrieves data from the View Handler's database storage.
// Returns DataResp to client
// responses:
//   200: dataGetResponse
// reads data from db and returns slice of Rows to caller
func (app *AppContext) GetStateDataHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	var statusCode = 200

	// Stub a data req to be populated from the body
	req := DataReq{}

	// Populate the data
	json.NewDecoder(r.Body).Decode(&req)

	resp := createDataResp(&req)

	if validDataRequest(&req) {
		data := data.New()
		err, dataList := data.SelectAllFromCurrent(req.Eui)
		displayPassFailMessage("Select All From Data Store", err, &req)
		if err != nil {
			statusCode = 400 // TODO(drt): Change to failure constant
		} else {
			// copy over each data element to data array
			for _, entry := range dataList {
				resp.Data = append(resp.Data, DataRecord{Ts: entry.Ts, ValueA: entry.ValueA, ValueB: entry.ValueB})
				//fmt.Printf("DEBUG 2: %v\n", resp.Data)
			}
		}
	} else {
		displayFailMessage("DataReq is not valid", &req)
		statusCode = 200 // TODO(drt): Change to failure constant
	}

	// Marshal provided interface into JSON strucutre
	respj, _ := json.Marshal(resp)

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	fmt.Fprintf(w, "%s", respj)
}

// TODO(drt) need to check if Eui is not emptry. Maybe add some other checks.
func validDataRequest(req *DataReq) bool {
	if req.Eui == "" {
		return false
	}
	return true
}

func createDataResp(req *DataReq) DataResp {
	resp := DataResp{}
	resp.TsStart = req.TsStart
	resp.TsEnd = req.TsEnd
	resp.Eui = req.Eui
	resp.Data = nil
	return resp
}

func displayPassFailMessage(message string, err error, req *DataReq) {

	if err == nil {
		fmt.Printf("PASS, EUI: %s, tsStart: %d, tsEnd: %d, %s\n", req.Eui, req.TsStart, req.TsEnd, message)
	} else {
		fmt.Printf("FAIL, EUI: %s, tsStart: %d, tsEnd: %d, %s, err: %s\n", req.Eui, req.TsStart, req.TsEnd, message, err)
	}
}

func displayFailMessage(message string, req *DataReq) {

	fmt.Printf("FAIL, EUI: %s, tsStart: %d, tsEnd: %d, %s\n", req.Eui, req.TsStart, req.TsEnd, message)
}

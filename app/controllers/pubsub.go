package controllers

import (
	"fmt"

	"bitbucket.org/cornjacket/iot/serverlib/pktproc2"
)

func (app *AppContext) initializePubSub(kafkaURL string) {

	fmt.Println("init pubsub ... !!")
	topic := "event_view_packet"
	groupID := ""
	NWorkers := 100

	err := pktproc2.NewKafkaConsumerWorkerPool(kafkaURL, topic, groupID, NWorkers, app.packetHandler)
	if err != nil {
		fmt.Println("init pubsub error invoking NewKafkaConsumerWorkerPool ... !!")
	}

}

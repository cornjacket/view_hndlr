package controllers

// purpose: test controller that responds to GET /nodestate/:eui and returns nodestate row

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages

	"bitbucket.org/cornjacket/view_hndlr/app/responses"
	nodestate "bitbucket.org/cornjacket/view_hndlr/app/view_nodestate_appnum4appapi3sysapi6"
	"github.com/julienschmidt/httprouter"
)

// A nodestate.Row is returned in the response
// swagger:response nodestateGetResponse
type nodestateGetResponseWrapper struct {
	// Nodestate received from View Handler
	// in: body
	Body nodestate.Row
}

// previously productEUIParameterWrapper

// swagger:parameters getNodeState
type nodestateEUIParameterWrapper struct {
	// The EUI of the node to retrieve its state from the database
	// in : path
	// required: true
	Eui string `json:"eui"`
}

// swagger:route GET /nodestate/{eui} nodestate getNodeState
// Retrieves nodestate from the View Handler.
// Returns nodeState Row to client
// responses:
//   200: nodestateGetResponse
func (app *AppContext) GetNodeState(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	eui := p.ByName("eui")
	fmt.Printf("GetNodeState() invoked with eui: %s\n", eui)

	ns, err := app.fetchNodeState(eui)
	fmt.Printf("TEST, GetNodeState: eui: %s\n", ns.Eui)
	if err != nil {
		fmt.Printf("TODO(drt): Need to return an error to the client\n")
	}

	responses.JSON(w, 200, ns)

}

func (app *AppContext) fetchNodeState(eui string) (nodestate.Row, error) {

	// 3. Fetch node state based on packet's EUI, if not found, then we will create the Node state row
	nodeState := nodestate.New()
	nodeRow, err := nodeState.GetRowByEui(eui) // DRT - currently I think that is function bypasses the normal object behavior. Need to think about this more.
	if err != nil {
		fmt.Printf("FAIL, GetByEui: err: %s\n", err)
		return nodestate.Row{}, err
	}
	return nodeRow, err
}

// A nodestate.Row is returned in the response
// swagger:response nodestatePostResponse
type nodestatePostResponseWrapper struct {
	// Nodestate sent to View Handler
	// in: body
	Body nodestate.Row
}

// swagger:route POST /nodestate nodestate sendNodeState
// Sends nodestate to the View Handler for database storage.
// Returns nodeState.Row to client
// responses:
//   201: nodestatePostResponse
// This function returns the entry point for the post to /nodestate.
func (app *AppContext) ShadowNodeState(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	nodeState := nodestate.New()
	row := nodestate.Row{}

	json.NewDecoder(r.Body).Decode(&row) // TODO: Check for error

	fmt.Printf("ShadowNodeState() invoked. eui: %s\n", row.Eui)

	createRowIfNotFound := true
	lockRow := false // ATC code correctly locks and unlocks. Review that code and revisit this.
	err, _ := nodeState.GetByEui(row.Eui, createRowIfNotFound, lockRow)
	if err != nil {
		//fmt.Printf("FAIL, classId: %s, corrId: %d, worker %d GetByEui: err: %s\n", p.ClassId, p.CorrId, wID, err) <------- missing CorrId and ClasId - add to the header instead of having inside the packet...
		fmt.Printf("FAIL, GetByEui: err: %s\n", err)
	}

	update, err := nodeState.UpdateRow(&row)
	if err != nil {
		//fmt.Printf("FAIL, classId: %s, corrId: %d, worker %d ShadowNodeState: err: %s\n", p.ClassId, p.CorrId, wID, err) <------- missing CorrId and ClasId - add to the header instead of having inside the packet...
		fmt.Printf("FAIL, ShadowNodeState: err: %s\n", err)
	} else if !update {
		fmt.Printf("INFO, NodeState Update skipped because no columns were changed\n")
	}

	// Marshal provided interface into JSON strucutre
	nj, _ := json.Marshal(row) //nodeState) // will need to cheange this. dont want to reply back with the entire row

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", nj)

}

// Package classification View Handler API.
//
// Documentation for View Handler API
//
// Terms Of Service:
//
// there are no TOS at this moment, use at your own risk we take no responsibility
//
//     Schemes: http
//     Host: localhost
//     BasePath: /
//     Version: 0.1.1
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: David Taylor<taylor.david.ray@gmail.com> http://john.doe.com
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
// swagger:meta
package controllers

import (
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// TODO: change guild so that it doesn't use package level scope. Instead create a type which lives in the AppContext
type AppContext struct {
	Router *httprouter.Router
}

func (app *AppContext) Run(addr, kafkaHostname, kafkaPort string) {
	// Instantiate a new router
	app.Router = httprouter.New()

	var kafkaURL = kafkaHostname + ":" + kafkaPort
	var kafkaEnabled = false
	if kafkaHostname != "" {
		kafkaEnabled = true
		app.initializePubSub(kafkaURL)
	}
	app.initializeRoutes(kafkaEnabled)

	fmt.Printf("Listening to port %s\n", addr)
	log.Fatal(http.ListenAndServe(":"+addr, app.Router))
}

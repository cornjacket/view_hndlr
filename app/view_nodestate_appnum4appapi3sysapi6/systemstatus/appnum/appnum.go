package appnum

import (
  "strconv"
)

type AppNum struct {
  value int
  updated bool
}

func New() *AppNum {
  return &AppNum{}
}

func (x *AppNum) IsUpdated() bool {
  return x.updated
}

func (x *AppNum) Get() int {
  return x.value
}

func (x *AppNum) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *AppNum) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *AppNum) String() string {
  return "AppNum"
}

func (x *AppNum) Type() string {
  return "int"
}

func (x *AppNum) Modifiers() string {
  return "not null"
}

func (x *AppNum) QueryValue() string {
  return strconv.Itoa(x.value)
}


package appapi

import (
  "strconv"
)

type AppApi struct {
  value int
  updated bool
}

func New() *AppApi {
  return &AppApi{}
}

func (x *AppApi) IsUpdated() bool {
  return x.updated
}

func (x *AppApi) Get() int {
  return x.value
}

func (x *AppApi) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *AppApi) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *AppApi) String() string {
  return "AppApi"
}

func (x *AppApi) Type() string {
  return "int"
}

func (x *AppApi) Modifiers() string {
  return "not null"
}

func (x *AppApi) QueryValue() string {
  return strconv.Itoa(x.value)
}


package lastpktrcvd

import (
)

type LastPktRcvd struct {
  value string
  updated bool
}

func New() *LastPktRcvd {
  return &LastPktRcvd{}
}

func (x *LastPktRcvd) IsUpdated() bool {
  return x.updated
}

func (x *LastPktRcvd) Get() string {
  return x.value
}

func (x *LastPktRcvd) Set(value string) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastPktRcvd) Load(value string) {
  x.value = value
  x.updated = false
}

func (x *LastPktRcvd) String() string {
  return "LastPktRcvd"
}

func (x *LastPktRcvd) Type() string {
  return "varchar(22)"
}

func (x *LastPktRcvd) Modifiers() string {
  return "not null"
}

func (x *LastPktRcvd) QueryValue() string {
  if x.value == "" {
    return "0"
  }
  return "\"" + x.value + "\""
}


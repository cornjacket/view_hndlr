package lastvoltagercvd

import (
  "strconv"
)

type LastVoltageRcvd struct {
  value int
  updated bool
}

func New() *LastVoltageRcvd {
  return &LastVoltageRcvd{}
}

func (x *LastVoltageRcvd) IsUpdated() bool {
  return x.updated
}

func (x *LastVoltageRcvd) Get() int {
  return x.value
}

func (x *LastVoltageRcvd) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *LastVoltageRcvd) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *LastVoltageRcvd) String() string {
  return "LastVoltageRcvd"
}

func (x *LastVoltageRcvd) Type() string {
  return "int"
}

func (x *LastVoltageRcvd) Modifiers() string {
  return "not null"
}

func (x *LastVoltageRcvd) QueryValue() string {
  return strconv.Itoa(x.value)
}


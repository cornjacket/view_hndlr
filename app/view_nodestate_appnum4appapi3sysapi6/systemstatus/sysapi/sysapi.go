package sysapi

import (
  "strconv"
)

type SysApi struct {
  value int
  updated bool
}

func New() *SysApi {
  return &SysApi{}
}

func (x *SysApi) IsUpdated() bool {
  return x.updated
}

func (x *SysApi) Get() int {
  return x.value
}

func (x *SysApi) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *SysApi) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *SysApi) String() string {
  return "SysApi"
}

func (x *SysApi) Type() string {
  return "int"
}

func (x *SysApi) Modifiers() string {
  return "not null"
}

func (x *SysApi) QueryValue() string {
  return strconv.Itoa(x.value)
}


package tslotvalid

import (
  "strconv"
)

type TSlotValid struct {
  value int
  updated bool
}

func New() *TSlotValid {
  return &TSlotValid{}
}

func (x *TSlotValid) IsUpdated() bool {
  return x.updated
}

func (x *TSlotValid) Get() int {
  return x.value
}

func (x *TSlotValid) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *TSlotValid) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *TSlotValid) String() string {
  return "TSlotValid"
}

func (x *TSlotValid) Type() string {
  return "tinyint"
}

func (x *TSlotValid) Modifiers() string {
  return "not null"
}

func (x *TSlotValid) QueryValue() string {
  return strconv.Itoa(x.value)
}


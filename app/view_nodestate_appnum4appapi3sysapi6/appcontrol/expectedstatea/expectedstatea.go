package expectedstatea

import (
  "strconv"
)

type ExpectedStateA struct {
  value int
  updated bool
}

func New() *ExpectedStateA {
  return &ExpectedStateA{}
}

func (x *ExpectedStateA) IsUpdated() bool {
  return x.updated
}

func (x *ExpectedStateA) Get() int {
  return x.value
}

func (x *ExpectedStateA) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *ExpectedStateA) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *ExpectedStateA) String() string {
  return "ExpectedStateA"
}

func (x *ExpectedStateA) Type() string {
  return "int"
}

func (x *ExpectedStateA) Modifiers() string {
  return "not null"
}

func (x *ExpectedStateA) QueryValue() string {
  return strconv.Itoa(x.value)
}


package actualstateb

import (
  "strconv"
)

type ActualStateB struct {
  value int
  updated bool
}

func New() *ActualStateB {
  return &ActualStateB{}
}

func (x *ActualStateB) IsUpdated() bool {
  return x.updated
}

func (x *ActualStateB) Get() int {
  return x.value
}

func (x *ActualStateB) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *ActualStateB) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *ActualStateB) String() string {
  return "ActualStateB"
}

func (x *ActualStateB) Type() string {
  return "int"
}

func (x *ActualStateB) Modifiers() string {
  return "not null"
}

func (x *ActualStateB) QueryValue() string {
  return strconv.Itoa(x.value)
}


package dataproc

import "fmt"

var WorkerQueue chan chan WorkRequest

func StartDispatcher(nworkers int) {
	// First, initialize the channel we are going to but the workers' work channels into.
	WorkerQueue = make(chan chan WorkRequest, nworkers)

	fmt.Printf("DATAPROC: Starting worker: ")
	// Now, create all of our workers.
	for i := 0; i < nworkers; i++ {
		fmt.Printf("%d ", i+1)
		worker := NewWorker(i+1, WorkerQueue)
		worker.Start()
	}
	fmt.Printf("\n")
	fmt.Println()

	go func() {
		for {
			select {
			case work := <-WorkQueue:
				if verbose {
					fmt.Println("DATAPROC: Received work requeust")
				}
				go func() {
					worker := <-WorkerQueue

					if verbose {
						fmt.Println("DATAPROC: Dispatching work request")
					}
					worker <- work
				}()
			}
		}
	}()
}

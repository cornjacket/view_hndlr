package dataproc

import (
	"fmt"
)

var dataProcFunc func(int, Data) bool
var dataProcFuncIsSet bool = false

// NewWorker creates, and returns a new Worker object. Its only argument
// is a channel that the worker can add itself to whenever it is done its
// work.
func NewWorker(id int, workerQueue chan chan WorkRequest) Worker {
	// Create, and return the worker.
	worker := Worker{
		ID:          id,
		Work:        make(chan WorkRequest),
		WorkerQueue: workerQueue,
		QuitChan:    make(chan bool)}

	return worker
}

func registerDataProcHandler(fp func(int, Data) bool) {
    dataProcFunc = fp
    dataProcFuncIsSet = true
    fmt.Printf("DATAPROC: RegisterDataProcHandler invoked\n")
}


type Worker struct {
	ID          int
	Work        chan WorkRequest
	WorkerQueue chan chan WorkRequest
	QuitChan    chan bool
}

// This function "starts" the worker by starting a goroutine, that is
// an infinite "for-select" loop.
func (w *Worker) Start() {
	go func() {
		for {
			// Add ourselves into the worker queue.
			w.WorkerQueue <- w.Work

			select {
			case work := <-w.Work:
				// Receive a work request.
				if verbose {
					fmt.Printf("DATAPROC: worker%d: Received notify request %v\n", w.ID, work.Data)
				}

				if dataProcFuncIsSet == true {
					if verbose {
						fmt.Printf("worker: pre dataProcFunc() invoked.\n")
					}
					dataProcFunc(w.ID, work.Data)
				} else {
					fmt.Printf("DATAPROC: worker%d: ERROR: dataProcFunc is not set.\n", w.ID)

				}

			case <-w.QuitChan:
				// We have been asked to stop.
				fmt.Printf("DATAPROC: worker%d stopping\n", w.ID)
				return
			}
		}
	}()
}

// Stop tells the worker to stop listening for work requests.
//
// Note that the worker will only stop *after* it has finished its work.
func (w *Worker) Stop() {
	go func() {
		w.QuitChan <- true
	}()
}

package dataproc

type WorkRequest struct {
	IsTest bool
	Data Data
}

package dataproc

import (
	// Standard library packets
	"encoding/json"
	"fmt"
	"net/http"

	// Third party packages
	"github.com/julienschmidt/httprouter"
	"bitbucket.org/cornjacket/iot/loriot"
)

type Data struct {
        Eui     string // this should be intrinsic_id, we can have LoraEui and Eui (intrinsic)
        Ts      uint64
        ValueA  int
        ValueB  int
        CorrId  uint64  `json:"corrid"`
        ClassId string  `json:"classid"`
        Hops 	int  	`json:"hops"`
}


var verbose = false

func NewDataHandlerFunc(NWorkers int, fp func(int, Data) bool) func(http.ResponseWriter, *http.Request, httprouter.Params) {

	// Start the dispatcher.
	fmt.Printf("DATAPROC\tNEW\tINFO\tStarting the dispatcher\n")
	StartDispatcher(NWorkers)

	// register data handler
	registerDataProcHandler(fp)

	return dataHandler 
}

// CreateData creates a new data resource
// returns the original data so that testing can confirm the data was received - this is not necessary.
func dataHandler(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	// Stub a data to be populated from the body
	d := Data{}

	// Populate the data
	json.NewDecoder(r.Body).Decode(&d)

	// Marshal provided interface into JSON strucutre
	dj, _ := json.Marshal(d)

	work := WorkRequest{Data: d}

	// Push the work onto the queue.
	WorkQueue <- work
	//fmt.Println("Work request queued")
	if verbose {
		fmt.Printf("DATAPROC\tCREATE\tINFO\tWork Request Queued: %v\n", work)
	}

	// Write content-type, statuscode, payload
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(201)
	fmt.Fprintf(w, "%s", dj)
}

func DisplayPassFailMessageWithLatency(message string, err error, wID int, d *Data, verbose bool) {

        if err != nil {
                fmt.Printf("FAIL, classId: %s, corrId: %d, EUI: %s, DataWorker %d, %s, hops: %d, err: %s\n", d.ClassId, d.CorrId, d.Eui, wID, message, d.Hops, err)
        } else if verbose {
                now := loriot.Now()
                latency := now - d.Ts
                fmt.Printf("PASS, classId: %s, corrId: %d, EUI: %s, DataWorker %d, %s, hops: %d, ts: %d, ts0: %d, lat: %d\n",
                        d.ClassId, d.CorrId, d.Eui, wID, message, d.Hops, now, d.Ts, latency)
        }

}

func DisplayStatus(wID int, d *Data) {

        fmt.Printf("INFO, classId: %s, corrId: %d, EUI: %s, DataWorker %d, Status: A: %d, B: %d, Hops: %d\n", d.ClassId, d.CorrId, d.Eui, wID, d.ValueA, d.ValueB, d.Hops)

}

package data_appnum4_appapi3_sysapi6

import (
  // Standard library packets
  "fmt"
  "os"
  "strconv"
  "database/sql"
  _ "github.com/go-sql-driver/mysql"
  "time"
  "bitbucket.org/cornjacket/view_hndlr/app/data_appnum4_appapi3_sysapi6/main/id"
  "bitbucket.org/cornjacket/view_hndlr/app/data_appnum4_appapi3_sysapi6/main/eui"
  "bitbucket.org/cornjacket/view_hndlr/app/data_appnum4_appapi3_sysapi6/main/createdat"
  "bitbucket.org/cornjacket/view_hndlr/app/data_appnum4_appapi3_sysapi6/main/updatedat"
  "bitbucket.org/cornjacket/view_hndlr/app/data_appnum4_appapi3_sysapi6/main/ts"
  "bitbucket.org/cornjacket/view_hndlr/app/data_appnum4_appapi3_sysapi6/main/valuea"
  "bitbucket.org/cornjacket/view_hndlr/app/data_appnum4_appapi3_sysapi6/main/valueb"
)

type Row struct {
  Id	int64	`json:"id"`
  Eui	string	`json:"eui"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  Ts	int64	`json:"ts"`
  ValueA	int	`json:"valuea"`
  ValueB	int	`json:"valueb"`
}

type PrevRow struct {
  Id	int64	`json:"id"`
  Eui	string	`json:"eui"`
  CreatedAt	time.Time	`json:"createdat"`
  UpdatedAt	time.Time	`json:"updatedat"`
  Ts	int64	`json:"ts"`
  ValueA	int	`json:"valuea"`
  ValueB	int	`json:"valueb"`
}

var db_conn *sql.DB

func Init(db *sql.DB) {
 fmt.Printf("Data_Appnum4_Appapi3_Sysapi6.Init() invoked\n")
  if db != nil {
    db_conn = db
  } else {
   fmt.Printf("Data_Appnum4_Appapi3_Sysapi6.Init() db = nil. Exiting...\n")
    os.Exit(0)
  }
  data_appnum4_appapi3_sysapi6 := New()
  //currentTable := data_appnum4_appapi3_sysapi6.GetCurrentTableName()
  //fmt.Printf("currentTable: %s, CreateTableName: %s\n", currentTable, data_appnum4_appapi3_sysapi6.CreateTableString())
  // Create table if it doesn't already exist
  err := data_appnum4_appapi3_sysapi6.CreateTable()
  if err != nil {
    fmt.Printf("data_appnum4_appapi3_sysapi6.CreateTable() failed. Assuming already exists.\n")
  } else {
    fmt.Printf("data_appnum4_appapi3_sysapi6.CreateTable() succeeded!!\n")
  }
}

type Data_Appnum4_Appapi3_Sysapi6 struct {
  db           *sql.DB
  tableName    string
  prevVersion  int
  version      int
  constraints  string
  Id	*id.Id
  Eui	*eui.Eui
  CreatedAt	*createdat.CreatedAt
  UpdatedAt	*updatedat.UpdatedAt
  Ts	*ts.Ts
  ValueA	*valuea.ValueA
  ValueB	*valueb.ValueB
}

func New() *Data_Appnum4_Appapi3_Sysapi6 {
  x := &Data_Appnum4_Appapi3_Sysapi6{}
  x.db = db_conn
  x.tableName = "data_appnum4_appapi3_sysapi6"
  x.prevVersion = 1
  x.version = 2
  x.Id=	id.New()
  x.Eui=	eui.New()
  x.CreatedAt=	createdat.New()
  x.UpdatedAt=	updatedat.New()
  x.Ts=	ts.New()
  x.ValueA=	valuea.New()
  x.ValueB=	valueb.New()
  x.constraints = "constraint pk_example primary key (id)"
  return x
}

var prevTableExists bool = false // TODO: Create Mechanism to auto detect if prevTable exists

func PrevTableExists() bool {
  return prevTableExists;
}

func (x *Data_Appnum4_Appapi3_Sysapi6) GetByEui (eui string, createIfNotFound bool) (error, bool) {
  fmt.Printf("table.GetByEui(): Entrance.\n")
  x.Eui.Load(eui)  // required: all rows must have a valid_onboard field that defaults to false, used to indicate if a field was created properly.
  // getByEui should select/search for entry in current version table
  err, rcvd := x.SelectFromCurrent(eui)
  // if entryfound then process_normally
  if err == nil && rcvd == true {
    fmt.Printf("table.GetByEui(): Returning nil, no error, rcvd one entry..\n")
    return nil, true
  }
  // else // no entry found
  fmt.Printf("table.GetByEui(): row DNE in current table.\n")
  if createIfNotFound == true {
    if PrevTableExists() {
      fmt.Printf("table.GetByEui(): checking prev table\n")
      err, rcvd = x.SelectFromPrev(eui)
      entryFound := (err == nil && rcvd == true)
      if entryFound {
//       copy over parameters from old row to new row, along with new row's new default fields, with valid_onboard set to true
        x.HandleMigrationOrRollback()
        err = x.insert()
        if err != nil {
          fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
        fmt.Printf("table.GetByEui(): x.insert succeeded.\n")
        err = x.DeleteByEui(x.GetPrevTableName())
        if err != nil {
          fmt.Printf("table.GetByEui(): x.delete failed from PrevTable.\n") // DRT - need to remove %s in guild
          return err, rcvd
        }
      } else {
//       create default row with valid_onboard = false -- How to do this?
        err = x.insert() // later we can just return this i think
        if err != nil {
          fmt.Printf("table.GetByEui(): x.insert failed.\n")
          return err, rcvd
        }
//       create default row with valid_onboard = false
      }
    } else { // later we can just return this i think
      err = x.insert()
      if err != nil {
        fmt.Printf("table.GetByEui(): x.insert failed.\n")
        return err, rcvd
      }
    }
  }
  return err, rcvd
}

func (x *Data_Appnum4_Appapi3_Sysapi6) HandleMigrationOrRollback() {
// currently doing nothing but this is where the migration or rollback logic should happen
// this should be generated code based on whether migration or rollback
fmt.Printf("table.HandleColumnsForMigrationOrRollback(): \n")
}

func (x *Data_Appnum4_Appapi3_Sysapi6) CreateTable() error {
  _, err := x.db.Exec(x.CreateTableString())
  return err
}

func (x *Data_Appnum4_Appapi3_Sysapi6) DeleteByEui(tableName string) error {
  _, err := x.db.Exec(x.DeleteString(tableName))
  return err
}

func (x *Data_Appnum4_Appapi3_Sysapi6) Insert(row *Row) error {
  x.Eui.Load(row.Eui)
  x.Ts.Load(row.Ts)
  x.ValueA.Load(row.ValueA)
  x.ValueB.Load(row.ValueB)
  return x.insert()
}

func (x *Data_Appnum4_Appapi3_Sysapi6) insert() error {
  insertString := x.InsertString()
  //fmt.Printf("table.InsertString(): %s\n", insertString)
  // insert is now not need with Eui
  insert, err := x.db.Exec(insertString,
   x.Eui.Get(),
   x.Ts.Get(),
   x.ValueA.Get(),
   x.ValueB.Get(),
  )
  if err != nil {
    return err
  }
  Id, err := insert.LastInsertId() // Id is not defined in EUI context, though it could be useful in other contexts
  if err != nil {
    return err
  }
  x.Id.Load(Id)
  //fmt.Printf("Insert id: %d\n", x.Id)
  return nil
}

// updated indicates whether any fields have changed and thus an update to the db was attempted 
func (x *Data_Appnum4_Appapi3_Sysapi6) Update() (anyFieldModified bool, err error) {
  updateString, anyFieldModified := x.UpdateString()
  fmt.Printf("table.UpdateString(): %s\n", updateString)

  if anyFieldModified {
    _, err = x.db.Exec(updateString)
  }
  if err != nil {
    return anyFieldModified, err
  }
  return anyFieldModified, nil
}

// ***************************************************************************************************
// 6.15.20 DRT I am adding this.
func (x *Data_Appnum4_Appapi3_Sysapi6) SelectAllFromCurrent(eui string) (err error, rows []Row) {
  //d.Id = id
  x.Eui.Load(eui)
  tableName := x.GetCurrentTableName()
  selectString := x.SelectString(tableName)
  //fmt.Printf("table.SelectString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, rows
  }
  defer results.Close()

  for results.Next() {
    var row Row
//err = results.Scan(&row.Id, &row.Eui, &row.SysApi, &row.AppApi)
    err = results.Scan( &row.Id, &row.Eui, &row.CreatedAt, &row.UpdatedAt, &row.Ts, &row.ValueA, &row.ValueB,)
  if err != nil {
  return err, rows
  }
  fmt.Printf("DEBUG(DRT) row: %s, ts: %d, valueA: %d, valueB: %d\n", row.Eui, row.Ts, row.ValueA, row.ValueB)

  rows = append(rows, row)

// Do the following have meaning any more when dealing with multiple entries? Do I need to create a slice of these data objects?
// DRT - The following statements have little meaning in the context of SelectAll. Is there a better abstraction?
 x.Id.Load(row.Id)
 x.Eui.Load(row.Eui)
 x.CreatedAt.Load(row.CreatedAt)
 x.UpdatedAt.Load(row.UpdatedAt)
 x.Ts.Load(row.Ts)
 x.ValueA.Load(row.ValueA)
 x.ValueB.Load(row.ValueB)

  }
  //fmt.Printf("rows: %v\n", rows)
  return err, rows 
}
// *************************************************************************************************************

func (x *Data_Appnum4_Appapi3_Sysapi6) SelectFromCurrent(eui string) (error, bool) { // later this will be SelectByEui(eui string)
  //d.Id = id
  x.Eui.Load(eui)
  tableName := x.GetCurrentTableName()
  selectString := x.SelectString(tableName) // we could pass eui into select for a nicer abstraction
  fmt.Printf("table.SelectString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()

  for results.Next() {
    var row Row
//err = results.Scan(&row.Id, &row.Eui, &row.SysApi, &row.AppApi)
    err = results.Scan( &row.Id, &row.Eui, &row.CreatedAt, &row.UpdatedAt, &row.Ts, &row.ValueA, &row.ValueB,)
  if err != nil {
  return err, false
  }
 x.Id.Load(row.Id)
 x.Eui.Load(row.Eui)
 x.CreatedAt.Load(row.CreatedAt)
 x.UpdatedAt.Load(row.UpdatedAt)
 x.Ts.Load(row.Ts)
 x.ValueA.Load(row.ValueA)
 x.ValueB.Load(row.ValueB)
    return nil, true
  }
  return nil, false
}

func (x *Data_Appnum4_Appapi3_Sysapi6) GetPrevTableName() string {
  return tableName(x.tableName, x.prevVersion)
}

//func (x *Data_Appnum4_Appapi3_Sysapi6) Select(id int64) error { // later this will be SelectByEui(eui string)
func (x *Data_Appnum4_Appapi3_Sysapi6) SelectFromPrev(eui string) (error, bool)  { // later this will be SelectByEui(eui string)
  //x.Id = id
  x.Eui.Load(eui)
  prevTable := x.GetPrevTableName()
  selectString := x.SelectString(prevTable) // we could pass eui into select for a nicer abstraction
  fmt.Printf("table.SelectString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()
  for results.Next() {
    var row PrevRow
    err = results.Scan( &row.Id, &row.Eui, &row.CreatedAt, &row.UpdatedAt, &row.Ts, &row.ValueA, &row.ValueB,)
    if err != nil {
       return err, false
    }
    x.Id.Load(row.Id)
    x.Eui.Load(row.Eui)
    x.CreatedAt.Load(row.CreatedAt)
    x.UpdatedAt.Load(row.UpdatedAt)
    x.Ts.Load(row.Ts)
    x.ValueA.Load(row.ValueA)
    x.ValueB.Load(row.ValueB)
    return nil, true
  }
  return nil, false
}

func (x *Data_Appnum4_Appapi3_Sysapi6) CreateTableString() string {
  return "create table " + x.GetCurrentTableName() +
  " ( " + x.Id.String() + " " + x.Id.Type() + " " + x.Id.Modifiers() +
  ", " + x.Eui.String() + " " + x.Eui.Type() + " " + x.Eui.Modifiers() +
  ", " + x.CreatedAt.String() + " " + x.CreatedAt.Type() + " " + x.CreatedAt.Modifiers() +
  ", " + x.UpdatedAt.String() + " " + x.UpdatedAt.Type() + " " + x.UpdatedAt.Modifiers() +
  ", " + x.Ts.String() + " " + x.Ts.Type() + " " + x.Ts.Modifiers() +
  ", " + x.ValueA.String() + " " + x.ValueA.Type() + " " + x.ValueA.Modifiers() +
  ", " + x.ValueB.String() + " " + x.ValueB.Type() + " " + x.ValueB.Modifiers() +
  ", " + x.constraints + " );"
}

// used internally for creating table name with current or previous version
func tableName(name string, version int) string {
   return name + "_v" + strconv.Itoa(version)
}

func newDeployment(current_version int, previous_version int) bool {
  if current_version == (previous_version + 1) {
    return true;
  }
  return false;
}

func (x *Data_Appnum4_Appapi3_Sysapi6) GetCurrentTableName() string {
  return tableName(x.tableName, x.version)
}

func (x *Data_Appnum4_Appapi3_Sysapi6) SelectString(tableName string) string {
  return "select * from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
}

// insert is only done to the current table
func (x *Data_Appnum4_Appapi3_Sysapi6) InsertString() string {
  return "insert into " + x.GetCurrentTableName() +
   " (" + x.Eui.String() + " , " +  x.Ts.String() + " , " +  x.ValueA.String() + " , " +  x.ValueB.String() + ") values" +
   " (?"+ " , ?" + " , ?" + " , ?" + ");"
}

// function returns true if at least one field has been updated
func (x *Data_Appnum4_Appapi3_Sysapi6) UpdateString() (string, bool) {
  count := 0
  first := true
  return_str := "update " + x.GetCurrentTableName() + " set "
  if x.Eui.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Eui.String() + " = " + x.Eui.QueryValue()
    first = false
  }
  if x.Ts.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Ts.String() + " = " + x.Ts.QueryValue()
    first = false
  }
  if x.ValueA.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ValueA.String() + " = " + x.ValueA.QueryValue()
    first = false
  }
  if x.ValueB.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ValueB.String() + " = " + x.ValueB.QueryValue()
    first = false
  }
  return_str += " where Eui = " + x.Eui.QueryValue() + ";"
  if count > 0 {
    return return_str, true
  } else {
    return "", false
  }
}

func (x *Data_Appnum4_Appapi3_Sysapi6) DeleteString(tableName string) string {
  return "delete from " + tableName + " where eui = " + x.Eui.QueryValue() + ";"
}

func (x *Data_Appnum4_Appapi3_Sysapi6) DeleteByIdString(tableName string) string {
  return "delete from " + tableName + " where id = " + x.Id.QueryValue() + ";"
}

// function returns true if at least one field has been updated
func (x *Data_Appnum4_Appapi3_Sysapi6) UpdateByIdString() (string, bool) {
  count := 0
  first := true
  return_str := "update " + x.GetCurrentTableName() + " set "
  if x.Eui.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Eui.String() + " = " + x.Eui.QueryValue()
    first = false
  }
  if x.Ts.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.Ts.String() + " = " + x.Ts.QueryValue()
    first = false
  }
  if x.ValueA.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ValueA.String() + " = " + x.ValueA.QueryValue()
    first = false
  }
  if x.ValueB.IsUpdated() {
    count++
    if !first {
      return_str += ", "
    }
    return_str += x.ValueB.String() + " = " + x.ValueB.QueryValue()
    first = false
  }
  return_str += " where id = " + x.Id.QueryValue() + ";"
  if count > 0 {
    return return_str, true
  } else {
    return "", false
  }
}

// updated indicates whether any fields have changed and thus an update to the db was attempted 
func (x *Data_Appnum4_Appapi3_Sysapi6) UpdateById() (anyFieldModified bool, err error) {
  updateString, anyFieldModified := x.UpdateByIdString()
  if anyFieldModified {
    fmt.Printf("table.UpdateByIdString(): %s\n", updateString)

    _, err := x.db.Exec(updateString,
    )
    if err != nil {
      return anyFieldModified, err
    }
  } else {
    fmt.Printf("table.UpdateById: No columns modified\n")
  }
  return anyFieldModified, nil
}

func (x *Data_Appnum4_Appapi3_Sysapi6) SelectByIdFromCurrent(id int64) (error, bool) {  //d.Id = id
  x.Id.Load(id)
  tableName := x.GetCurrentTableName()
  selectString := x.SelectByIdString(tableName)
  fmt.Printf("table.SelectByIdString(): %s\n", selectString)
  results, err := x.db.Query(selectString)
  if err != nil {
    return err, false
  }
  defer results.Close()

  for results.Next() {
    var row Row
//err = results.Scan(&row.Id, &row.Eui, &row.SysApi, &row.AppApi)
    err = results.Scan( &row.Id, &row.Eui, &row.CreatedAt, &row.UpdatedAt, &row.Ts, &row.ValueA, &row.ValueB,)
  if err != nil {
  return err, false
  }
 x.Id.Load(row.Id)
 x.Eui.Load(row.Eui)
 x.CreatedAt.Load(row.CreatedAt)
 x.UpdatedAt.Load(row.UpdatedAt)
 x.Ts.Load(row.Ts)
 x.ValueA.Load(row.ValueA)
 x.ValueB.Load(row.ValueB)
    return nil, true
  }
  return nil, false
}

func (x *Data_Appnum4_Appapi3_Sysapi6) SelectByIdString(tableName string) string {
  return "select * from " + tableName + " where id = " + x.Id.QueryValue() + ";"
}


package valuea

import (
  "strconv"
)

type ValueA struct {
  value int
  updated bool
}

func New() *ValueA {
  return &ValueA{}
}

func (x *ValueA) IsUpdated() bool {
  return x.updated
}

func (x *ValueA) Get() int {
  return x.value
}

func (x *ValueA) Set(value int) {
  if x.value != value {
    x.value = value
    x.updated = true
  }
}

func (x *ValueA) Load(value int) {
  x.value = value
  x.updated = false
}

func (x *ValueA) String() string {
  return "ValueA"
}

func (x *ValueA) Type() string {
  return "int"
}

func (x *ValueA) Modifiers() string {
  return "not null"
}

func (x *ValueA) QueryValue() string {
  return strconv.Itoa(x.value)
}


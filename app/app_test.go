package app

import (
	"fmt"
	"log"
	"testing"
	"time"

	. "gopkg.in/check.v1"

	"bitbucket.org/cornjacket/iot/message"
	view_client "bitbucket.org/cornjacket/view_hndlr/client_lib"
)

var client view_client.ViewHndlrService

// Hook up gocheck into the "go test" runner.
func Test(t *testing.T) { TestingT(t) }

type TestSuite struct {
	AppService AppService
}

var _ = Suite(&TestSuite{})

func (s *TestSuite) SetUpSuite(c *C) {
	fmt.Println("SetUpSuite() invoked")
	s.AppService = NewAppService()
	s.AppService.InitEnv()
	s.AppService.DropTables()
	s.AppService.SetupDatabase()
	go s.AppService.Run()
	// TODO(drt) - I may need a time delay to guarantee that the server is up and talking to the database
	time.Sleep(1 * time.Second)
	config := view_client.ViewHndlrServiceConfig{
		TransportType: view_client.HttpPost,
		Hostname:      "localhost",
		Port:          s.AppService.Env.PortNum,
		PacketPath:    "/packet",
		NodestatePath: "/nodestate",
		DataPath:      "/data",
	}
	if err := client.Open(config); err != nil {
		//log.Fatal("client.Open() error: %s", err)
		log.Fatal("client.Open() failed")
	}

}

func (s *TestSuite) SetUpTest(c *C) {
	fmt.Println("SetupTest() invoked")
	s.AppService.CreateTables()
}

func (s *TestSuite) TearDownTest(c *C) {
	fmt.Println("TearDownTest() invoked")
	s.AppService.DropTables()
}

func (s *TestSuite) TearDownSuite(c *C) {
	fmt.Println("TearDownSuite() invoked")
	// TODO(drt) - close the db connection
}

func (s *TestSuite) TestDataPath(c *C) {
	// Send Data
	fmt.Printf("component_test.dataTest\n")
	data := view_client.Data{
		Eui:    "1234",
		Ts:     uint64(12345),
		ValueA: 1,
		ValueB: 1,
	}
	err := client.SendStatus(data) // This should change to SendData
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the view_hndlr is finished processing

	// Retrieve Data and Compare with Expected
	req := view_client.DataStateReq{
		TsStart: uint64(1),
		TsEnd:   uint64(100),
		Eui:     "1234",
	}
	dataResp, err := client.GetData(req)
	c.Assert(err, Equals, nil)
	if err == nil {
		c.Assert(dataResp.Eui, Equals, "1234")
		c.Assert(len(dataResp.Data), Equals, 1)
		c.Assert(dataResp.Data[0].ValueA, Equals, 1)
		c.Assert(dataResp.Data[0].ValueB, Equals, 1)
	}

}

func (s *TestSuite) TestNodeStatePath(c *C) {
	// Send NodeState
	fmt.Printf("component_test.nodestateTest\n")

	writeState := view_client.NodeState{
		Eui:          "1234",
		ValidOnboard: 1,
		//CreatedAt: 0,
		//UpdatedAt: 0,
		AppNum:                 4,
		AppApi:                 3,
		SysApi:                 6,
		AppFwVersion:           1,
		SysFwVersion:           1,
		ComApi:                 2,
		ComFwVersion:           3,
		LastPktRcvd:            "12345678",
		LastPktRcvdTs:          int64(10),
		LastVoltageRcvd:        200,
		LastVoltageRcvdTs:      int64(20),
		LastResetCause:         2,
		LastResetCauseTs:       int64(20),
		LastRssi:               -110,
		LastRssiTs:             int64(20),
		LastSnr:                152,
		LastSnrTs:              int64(20),
		NumDownstreams:         21,
		NumClearQueries:        4,
		AppLowPowerState:       2,
		GlobalDownstreamEnable: 1,
		SystemDownstreamEnable: 1,
		AppDownstreamEnable:    1,
		TSlot:                  4,
		TSlotValid:             1,
		TSlotAssignedTs:        20,
		ActualStateA:           1,
		ActualStateB:           0,
		ExpectedStateA:         1,
		ExpectedStateB:         0,
	}

	err := client.SendNodestate(&writeState)
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the view_hndlr is finished processing

	// Retrieve NodeState and Compare with Expected
	nodestate, err := client.GetNodeState("1234")

	c.Assert(err, Equals, nil)
	if err == nil {
		c.Assert(nodestate.Eui, Equals, "1234")
		c.Assert(nodestate.ActualStateA, Equals, 1)
		c.Assert(nodestate.ExpectedStateA, Equals, 1)
		c.Assert(nodestate.ActualStateB, Equals, 0)
		c.Assert(nodestate.ExpectedStateB, Equals, 0)
		c.Assert(nodestate.TSlot, Equals, 4)
		// TODO(drt) - add more assertions for all nodestate fields above
	}

}

func (s *TestSuite) TestPacketPath(c *C) {
	// Send Packet
	fmt.Printf("component_test.packetTest\n")

	p := message.UpPacket{
		Dr:   "?",
		Ts:   uint64(12345),
		Eui:  "1234",
		Ack:  false,
		Cmd:  "Rx",
		Snr:  12.4,
		Data: "12345678",
		Fcnt: 12,
		Freq: uint64(20),
		Port: 1,
		Rssi: -110,
	}
	err := client.SendPacket(p)
	c.Assert(err, Equals, nil)
	time.Sleep(100 * time.Millisecond) // This is needed so that the TestTearDown func doesn't drop the tables before the view_hndlr is finished processing

	// Get packet and compare with expected
	pktReq := view_client.PacketReq{
		TsStart: uint64(0),
		TsEnd:   uint64(100),
		Eui:     "1234",
	}

	pktResp, err := client.GetPackets(pktReq)

	c.Assert(err, Equals, nil)
	if err == nil {
		c.Assert(pktResp.Eui, Equals, "1234")
		c.Assert(len(pktResp.Packets), Equals, 1)
		c.Assert(pktResp.Packets[0].Data, Equals, "12345678")
	}

}

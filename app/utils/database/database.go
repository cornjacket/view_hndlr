package database 

import (
        "fmt"
	"log"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"bitbucket.org/cornjacket/view_hndlr/app/utils/pinger"
)

func CreateDatabaseIfNotExists(dbName string, dbUsername string, dbPassword string, dbHostname string, dbPort string) error {
	dbConnString := dbUsername + ":"+dbPassword+"@tcp("+dbHostname+":"+dbPort+")/"
	fmt.Printf("init\tdebug: %s\n", dbConnString)
	db, err := sql.Open("mysql", dbConnString)
        if err != nil {
                log.Fatal(err)
        }
	defer db.Close()
        err = pinger.PingExternalService(dbHostname, &pinger.DbPinger{db})
        if err != nil {
                log.Fatal(err)
        }

	createDbString := "CREATE DATABASE IF NOT EXISTS " + dbName + ";"
	fmt.Printf("init\tdebug: %s\n", createDbString)
	_, err = db.Exec(createDbString)
	//db.Close() <--- how to defer db.Close from main so that db is closed? Main needs to have control of closing the db
	return err
}


func DropTableIfExists(dbName, dbUsername, dbPassword, dbHostname, dbPort, tableName string) error {
	dbConnString := dbUsername + ":"+dbPassword+"@tcp("+dbHostname+":"+dbPort+")/"+dbName+"?parseTime=true"
	//fmt.Printf("init\tdebug: %s\n", dbConnString)
	db, err := sql.Open("mysql", dbConnString)
        if err != nil {
                log.Fatal(err)
        }
	defer db.Close()
        err = pinger.PingExternalService(dbHostname, &pinger.DbPinger{db})
        if err != nil {
                log.Fatal(err)
        }

	// This should be integrated into guild
	dropTableString := "DROP TABLE IF EXISTS "+tableName+";"
	//fmt.Printf("init\tdebug: %s\n", dropTableString)
	_, err = db.Exec(dropTableString)
	return err
}


component_test:
	go test	./...
mod_replace:
	go mod edit -replace bitbucket.org/cornjacket/iot=/home/david/work/go/src/bitbucket.org/cornjacket/iot
drop_mod_replace:
	go mod edit -dropreplace bitbucket.org/cornjacket/iot
docker_local_bash:
	docker exec -it local_view_hndlr /bin/ash
docker_run_prod:
	# this won't work as I expect because the docker container is not able to talk outside of its container space.
	# i think i need to use a docker-compose flow
	docker run -it --name=local_view_hndlr --env="DB_PASSWORD=abc" -p 8082:8082 cornjacket/iot_app_4_view_hndlr
docker_run_dev:
	# this won't work as I expect because the docker container is not able to talk outside of its container space.
	# i think i need to use a docker-compose flow
	docker run -it --name=local_view_hndlr -p 8082:8082 view_hndlr_dev 
docker_build_prod:
	docker build . -f ./docker/Dockerfile.prod -t cornjacket/iot_app_4_view_hndlr
docker_build_dev:
	docker build . -f ./docker/Dockerfile.dev -t view_hndlr_dev
mysql_run:
	docker run --rm --detach --name=my-mariadb --env="MYSQL_ROOT_PASSWORD=abc" --publish 3306:3306 --volume=/root/docker/my-mariadb/conf.d:/etc/mysql/conf.d --volume=/storage/docker/mariadb-data:/var/lib/mysql mariadb
mysql_stop:
	docker stop my-mariadb
kafka_up:
	docker-compose -f ./local_test/kafka/docker-compose.yaml up
kafka_down:
	docker-compose -f ./local_test/kafka/docker-compose.yaml down

